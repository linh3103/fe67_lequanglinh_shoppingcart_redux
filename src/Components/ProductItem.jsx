import React, { Component } from 'react';
import {connect} from 'react-redux';
import {createAction} from '../store/actions/index';

class ProductItem extends Component {
    handleViewDetail = () =>{
        this.props.dispatch(createAction("VIEW_DETAIL_PRODUCT",{
            product: this.props.product
        }));
    }
    handleAddToCart = () =>{
        this.props.dispatch(createAction("ADD_TO_CART", {
            product: this.props.product
        }));
    }
    render() {
        const product = this.props.product
        return (
            <div className="card p-3 mt-3">
                <img style={{height: 250, width: "100%"}} src = {product.img} alt="product"/>
                <h4>{product.name}</h4>
                <p>{product.price.toLocaleString()} vnd</p>
                <div>
                    <button onClick={this.handleViewDetail} className="btn btn-primary mr-2">Chi tiết</button>
                    <button onClick={this.handleAddToCart} className="btn btn-outline-primary">Thêm giỏ hàng</button>
                </div>
            </div>
        );
    }
}

export default connect() (ProductItem);
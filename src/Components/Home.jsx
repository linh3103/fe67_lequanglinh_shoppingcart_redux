import React, { Component } from 'react';
import Cart from './Cart';
import Detail from './Detail';
import ProductList from './ProductList';
import {connect} from 'react-redux'
class Home extends Component {
    render() {
        return (
            <div>
                <h1>Shopping cart react-redux</h1>
                <Cart/>
                <ProductList/>
                {
                    this.props.selectedProduct ? <Detail/> : null 
                }
            </div>
        );
    }
}

const mapStateToProps = (state) =>{
    return{
        selectedProduct: state.products.selectedProduct
    }
}

export default connect(mapStateToProps) (Home);
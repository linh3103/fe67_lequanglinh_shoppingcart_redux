import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createAction } from '../store/actions';

class Cart extends Component {
    renderCart = () =>{
        const cartHTML = this.props.productCart.map(item=>
            (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <img src={item.img} alt="product" style={{height: '50px', width: '50px'}}/>
                    </td>
                    <td>
                        <button onClick={() => this.handleCoungtin(item.id, false)} className="btn btn-danger">
                            <i className="fa fa-minus"></i>
                        </button>
                        <span className="mx-3">{item.count}</span>
                        <button onClick={() => this.handleCoungtin(item.id, true)} className="btn btn-success">
                            <i className="fa fa-plus"></i>
                        </button>
                    </td>
                    <td>{item.price.toLocaleString()} vnđ</td>
                    <td>{(item.price * item.count).toLocaleString()} vnđ</td>
                    <td>
                        <button onClick={() => this.handleDeleteCart(item.id)} className="btn btn-outline-danger">
                        <i class="fa fa-trash"></i>
                        </button>
                    </td>
                </tr>
            )
        );
        return cartHTML
    }

    handleDeleteCart = (id) =>{
        this.props.dispatch(createAction("DELETE_CART", {
            id
        }))
    }
    handleCoungtin = (id, tang_giam) =>{
        this.props.dispatch(createAction("TANG_GIAM_SOLUONG", {
            id,
            tang_giam
        }))
    }
    render() {
        const totalProductCart = this.props.productCart.reduce((total, item) => {
            return total += item.count
        }, 0)
        return (
            <div>
                <div>
                    <button style={{fontSize: 20}} type="button" className="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal">
                        Giỏ hàng
                            <sup>
                                <span style={{fontSize: 15}} className="badge badge-warning">{totalProductCart}</span>
                            </sup>
                    </button>
                    <div className="modal fade" id="exampleModal" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog modal-xl">
                        <div className="modal-content">
                            <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            </div>
                            <div className="modal-body">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>Mã sản phẩm</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Hình ảnh</th>
                                        <th>Số lượng</th>
                                        <th>Đơn giá</th>
                                        <th>Thành tiền</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderCart()}
                                </tbody>
                            </table>
                            </div>
                            <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) =>{
    return {
        productCart: state.cart.cart
    }
}
export default connect(mapStateToProps) (Cart);
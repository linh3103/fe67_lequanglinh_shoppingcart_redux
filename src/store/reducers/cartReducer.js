const inititalState = {
    cart: []
}

const reducer = (state = inititalState, action) =>{
    switch (action.type) {
        case "ADD_TO_CART":{
            const cloneCart = [...state.cart];
            const cloneProduct = {...action.payload.product, count: 1}
            const index = cloneCart.findIndex(item => item.id ===cloneProduct.id);
            if(index !== -1){
                cloneCart[index].count ++;
            }else{
                cloneCart.push(cloneProduct)
            }
            state.cart = cloneCart;
            return {...state};
        }

        case "DELETE_CART":{
            const cloneCartDel = [...state.cart];
            const delIndex = cloneCartDel.findIndex(item => item.id === action.payload.id);
            if(delIndex !== -1){
                cloneCartDel.splice(delIndex, 1);
            }
            state.cart = cloneCartDel;
            return {...state}
        }

        case "TANG_GIAM_SOLUONG":{
            const cloneCartCounting = [...state.cart];
            const countIndex = cloneCartCounting.findIndex(item => item.id === action.payload.id);
            if(action.payload.tang_giam){
                cloneCartCounting[countIndex].count ++
            }else{
                if(cloneCartCounting[countIndex].count > 1){
                    cloneCartCounting[countIndex].count --
                }
            }
            state.cart = cloneCartCounting;
            return {...state}
        }
        default:
            return state;
    }
}

export default reducer
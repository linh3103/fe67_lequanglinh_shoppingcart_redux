import React, { Component } from 'react';
import {connect} from 'react-redux'
class Detail extends Component {
    render() {
        const selectedProduct = this.props.selectedProduct;
        return (
            <div className="container my-5 pb-3 border border-dark rounded">
                <h3 className="py-3">Chi tiết sản phẩm</h3>
                <div className="row">
                    <div className="col-4">
                    <div className="card" style={{width: '18rem'}}>
                        <img src={selectedProduct.img} className="card-img-top" alt="..." />
                    </div>
                    </div>
                    <div className="col-8">
                        <table className="table text-left">
                            <tr>
                                <td>Tên sản phẩm</td>
                                <td>{selectedProduct.name}</td>
                            </tr>
                            <tr>
                                <td>Màn hình</td>
                                <td>{selectedProduct.screen}</td>
                            </tr>
                            <tr>
                                <td>Camera trước</td>
                                <td>{selectedProduct.backCamera}</td>
                            </tr>
                            <tr>
                                <td>Camera sau</td>
                                <td>{selectedProduct.frontCamera}</td>
                            </tr>
                            <tr>
                                <td>Mô tả</td>
                                <td>{selectedProduct.desc}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) =>{
    return {
        selectedProduct: state.products.selectedProduct
    }
}

export default connect(mapStateToProps) (Detail);
import React, { Component } from 'react';
import {connect} from 'react-redux'
import ProductItem from './ProductItem';
class ProductList extends Component {
    renderProduct = () =>{
       const productHTML =  this.props.products.map(item => 
            <div  key={item.id} className="col-xs-12 col-sm-6 col-xl-3 px-2">
                <ProductItem product = {item}/>
            </div>
        )
        return productHTML
    }
    render() {
        return (
            <div className="container">
                <div class="row">
                    {this.renderProduct()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) =>{
    return{
        products: state.products.products
    }
}

export default connect(mapStateToProps) (ProductList);